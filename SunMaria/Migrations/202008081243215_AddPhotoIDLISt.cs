﻿namespace SunMaria.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPhotoIDLISt : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Items", "PhotoID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Items", "PhotoID", c => c.Int(nullable: false));
        }
    }
}
