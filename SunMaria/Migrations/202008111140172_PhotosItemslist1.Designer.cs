﻿// <auto-generated />
namespace SunMaria.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.0")]
    public sealed partial class PhotosItemslist1 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(PhotosItemslist1));
        
        string IMigrationMetadata.Id
        {
            get { return "202008111140172_PhotosItemslist1"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
