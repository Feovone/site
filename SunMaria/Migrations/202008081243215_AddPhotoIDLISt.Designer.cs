﻿// <auto-generated />
namespace SunMaria.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.0")]
    public sealed partial class AddPhotoIDLISt : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddPhotoIDLISt));
        
        string IMigrationMetadata.Id
        {
            get { return "202008081243215_AddPhotoIDLISt"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
