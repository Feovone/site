﻿namespace SunMaria.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initialDb : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Items",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Collection = c.String(),
                        ForWhom = c.String(),
                        Age = c.String(),
                        TypeCanvas = c.String(),
                        Warehouse = c.String(),
                        Care = c.String(),
                        Season = c.String(),
                        Description = c.String(),
                        IsAvailable = c.Boolean(nullable: false),
                        CategoriesID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categories", t => t.CategoriesID, cascadeDelete: true)
                .Index(t => t.CategoriesID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Items", "CategoriesID", "dbo.Categories");
            DropIndex("dbo.Items", new[] { "CategoriesID" });
            DropTable("dbo.Items");
            DropTable("dbo.Categories");
        }
    }
}
