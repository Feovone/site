﻿namespace SunMaria.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PhotosItemslist : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Items", "Photos_Id", "dbo.Photos");
            DropIndex("dbo.Items", new[] { "Photos_Id" });
            DropColumn("dbo.Items", "Photos_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Items", "Photos_Id", c => c.Int());
            CreateIndex("dbo.Items", "Photos_Id");
            AddForeignKey("dbo.Items", "Photos_Id", "dbo.Photos", "Id");
        }
    }
}
