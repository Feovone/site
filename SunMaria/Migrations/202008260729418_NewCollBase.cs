﻿namespace SunMaria.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NewCollBase : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Categories", "Subgroup", c => c.String());
            AddColumn("dbo.Items", "Category1", c => c.String());
            AddColumn("dbo.Items", "Category2", c => c.String());
            AddColumn("dbo.Items", "Category3", c => c.String());
            AddColumn("dbo.Items", "Category4", c => c.String());
            AddColumn("dbo.Items", "Category5", c => c.String());
            DropColumn("dbo.Items", "Collection");
            DropColumn("dbo.Items", "TypeCanvas");
            DropColumn("dbo.Items", "Warehouse");
            DropColumn("dbo.Items", "Care");
            DropColumn("dbo.Items", "Season");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Items", "Season", c => c.String());
            AddColumn("dbo.Items", "Care", c => c.String());
            AddColumn("dbo.Items", "Warehouse", c => c.String());
            AddColumn("dbo.Items", "TypeCanvas", c => c.String());
            AddColumn("dbo.Items", "Collection", c => c.String());
            DropColumn("dbo.Items", "Category5");
            DropColumn("dbo.Items", "Category4");
            DropColumn("dbo.Items", "Category3");
            DropColumn("dbo.Items", "Category2");
            DropColumn("dbo.Items", "Category1");
            DropColumn("dbo.Categories", "Subgroup");
        }
    }
}
