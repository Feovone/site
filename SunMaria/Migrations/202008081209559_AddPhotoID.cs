﻿namespace SunMaria.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPhotoID : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Photos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PhotoId = c.Int(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Items", "PhotoID", c => c.Int(nullable: false));
            AddColumn("dbo.Items", "Photos_Id", c => c.Int());
            CreateIndex("dbo.Items", "Photos_Id");
            AddForeignKey("dbo.Items", "Photos_Id", "dbo.Photos", "Id");
            DropColumn("dbo.Items", "PhotoPath");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Items", "PhotoPath", c => c.String());
            DropForeignKey("dbo.Items", "Photos_Id", "dbo.Photos");
            DropIndex("dbo.Items", new[] { "Photos_Id" });
            DropColumn("dbo.Items", "Photos_Id");
            DropColumn("dbo.Items", "PhotoID");
            DropTable("dbo.Photos");
        }
    }
}
