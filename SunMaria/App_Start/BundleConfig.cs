﻿using System.Web;
using System.Web.Optimization;

namespace SunMaria
{
    public class BundleConfig
    {
        // Дополнительные сведения об объединении см. на странице https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.min.js",
                      "~/Scripts/popper.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/indexJs").Include(
                      "~/Scripts/bootstrap.min.js",
                      "~/Scripts/popper.min.js"));
            
            bundles.Add(new StyleBundle("~/bundles/items").Include(
                        "~/Content/style.css"
                       ));

            bundles.Add(new StyleBundle("~/bundles/index").Include(
                        "~/Content/Contact-Form-Clean.css",
                        "~/Content/styles.css",
                        "~/Content/index.css"
                       )); 

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.min.js",
                      "~/Content/bootstrap.min.сss",
                      "~/Content/styles.css",
                      "~/Content/ionicons.min.css"
                     ));
        }
    }
}
