﻿using SunMaria.Areas.Item.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SunMaria.Models
{
    public class Cart
    {
        private List<CartLine> lineCollection = new List<CartLine>();
        public IEnumerable<CartLine> Lines { get { return lineCollection; } }
        public void AddItem(Items item, int quantity)
        {
            CartLine line = lineCollection
                .Where(i => i.Item.Id == item.Id)
                .FirstOrDefault();
            if(line == null)
            {
                lineCollection.Add(new CartLine { Item = item, Quantity = quantity });
            }
            else
            {
                line.Quantity += quantity;
            }
        }
        public void RemoveLine(Items item)
        {
            lineCollection.RemoveAll(l => l.Item.Id == item.Id);
        }
        public decimal ComputeTotalValue()
        {
            return lineCollection.Sum(e => e.Item.Price * e.Quantity);
        }
        public void Clear()
        {
            lineCollection.Clear();
        }
    }
    public class CartLine
    {
        public Items Item { get; set; }
        public int Quantity { get; set; }
    }
}