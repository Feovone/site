﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace SunMaria.Areas.Item.Models
{
    public class ItemAdd
    {
        [Required]
        public string Name { get; set; }
        public string Collection { get; set; }
        public string ForWhom { get; set; }
        public string Age { get; set; }
        public string TypeCanvas { get; set; }
        public string Warehouse { get; set; }
        public string Care { get; set; }
        public string Season { get; set; }
        public int Price { get; set; }
        public string Description { get; set; }
        [Required]
        public bool IsAvailable { get; set; }
        [Required]
        public int CategoriesID { get; set; }

    }
}