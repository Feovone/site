﻿using SunMaria.Areas.Item.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SunMaria.Areas.Item.Interfaces
{
    public interface IItemsCategory
    {
        IEnumerable<Categories> AllCategories { get; }
    }
}
