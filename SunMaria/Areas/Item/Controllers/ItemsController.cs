﻿using SunMaria.Areas.Item.Interfaces;
using SunMaria.Areas.Item.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace SunMaria.Areas.Item.Controllers
{
    public class ItemsController : Controller
    {
        private readonly IAllItems _allItems;
        private readonly IItemsCategory _allCategories;
        ItemContext context = new ItemContext();
        public ItemsController(IAllItems allItems, IItemsCategory allCategories)
        {
            _allItems = allItems;
            _allCategories = allCategories;
        }
        public ActionResult List(string category = "All")
        {
            List<Items> item;
            ItemsListViewModel obj = new ItemsListViewModel();
            if (category != "All")
            {
                IEnumerable<string> categories;
                categories = category.Split(' ');
                item = context.Item.Where(u => u.Categories.Name == categories.FirstOrDefault(c => c == u.Categories.Name)).ToList();
                if (item.Count() == 0) { item = context.Item.Include(x => x.Photo).Take(2).ToList(); }
            }
            else
            { 
              item = context.Item.Include(x => x.Photo).ToList(); 
            }

            return View(item);
        }
        public ViewResult Id(int id = 0)
        {
            Items item = context.Item.Include(x => x.Photo).First(x => x.Id == id);
            //ItemsListViewModel obj = new ItemsListViewModel();
            //obj.AllItems = _allItems.Items;
            //obj.currCategory = "";
            //obj.Ids = id;
            //ViewBag.item = item;
            return View("~/Views/Items/Id.cshtml", item);
        }
        [Authorize]
        public ActionResult Add()
        {
            SelectList selectCategories = new SelectList(context.Categories, "Id", "Name");
            ViewBag.categories = selectCategories;
            return View(); }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(Items model)
        {
            if (ModelState.IsValid)
            {
                Items item = null;
                using (ItemContext db = new ItemContext())
                {
                    item = db.Item.FirstOrDefault(u => u.Name == model.Name);
                }
                if (item == null)
                {
                    string photoPath = "";
                    //if (Request.Files.Count > 0)
                    //{
                    //    HttpFileCollection attachments = Request.Files;
                    //    for (int i = 0; i < attachments.Count; i++)
                    //    {
                    //        HttpPostedFile attachment = attachments[i];
                    //        if (attachment.ContentLength > 0 && !String.IsNullOrEmpty(attachment.FileName))
                    //        {
                    //            //do your file saving or any related tasks here.
                    //        }
                    //    }
                    //}
                    using (ItemContext db = new ItemContext())
                    {
                        Items PhotoId = db.Item.OrderByDescending(x => x.Id).FirstOrDefault();
                        Photos lastPhoto;
                        List<int> photoID = new List<int>();
                        HttpFileCollectionBase files = Request.Files;
                        lastPhoto = db.Photos.OrderByDescending(x => x.Id).FirstOrDefault();
                        for (int i = 0; i < files.Count; i++)
                        {
                            HttpPostedFileBase file = files[i];
                            file.SaveAs(Server.MapPath("~/Images/" + file.FileName));
                            db.Photos.Add(new Photos
                            {
                                Name = file.FileName,
                                //PhotoId = PhotoId.Id
                            });

                            photoID.Add(lastPhoto.Id + 1 + i);
                        }

                        db.Item.Add(new Items
                        {
                            Name = model.Name,
                            ForWhom = model.ForWhom,
                            Age = model.Age,
                            Category1 = model.Category1,
                            Category2 = model.Category2,
                            Category3 = model.Category3,
                            Category4 = model.Category4,
                            Category5 = model.Category5,
                            Description = model.Description,
                            IsAvailable = model.IsAvailable,
                            CategoriesID = model.CategoriesID,
                            Price = model.Price,
                        });
                        db.SaveChanges();
                        item = db.Item.Where(i => i.Name == model.Name).FirstOrDefault();
                    }
                    if (item != null)
                    {
                        return RedirectToAction("List", "Items");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Товар с таким названием уже существует");
                }



            }
            return View(model);
        }
    }
}
