﻿using SunMaria.Areas.Item.Interfaces;
using SunMaria.Areas.Item.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SunMaria.Areas.Item.Repository
{
    public class CategoryRepository : IItemsCategory
    {
        private readonly ItemContext itemContext;
        public CategoryRepository(ItemContext itemContext)
        {
            this.itemContext = itemContext;
        }
        public IEnumerable<Categories> AllCategories =>itemContext.Categories;
    }
}