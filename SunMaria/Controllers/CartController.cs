﻿using SunMaria.Areas.Item.Models;
using SunMaria.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SunMaria.Controllers
{
    public class CartController : Controller
    {
        private readonly ItemContext db = new ItemContext();
        public ViewResult Index(string returnUrl)
        {
            return View(new CartIndexViewModel
            {
                Cart = GetCart(),
                ReturnUrl = returnUrl
            }); 
        }
        public Cart GetCart()
        {
            Cart cart = (Cart)Session["Cart"];
            if(cart == null)
            {
                cart = new Cart();
                Session["Cart"] = cart;
            }
            return cart;
        }
        public RedirectToRouteResult AddToCart(int itemId, string returnUrl)
        {
            Items item = db.Item
                .FirstOrDefault(i => i.Id == itemId);
            if(item != null)
            {
                GetCart().AddItem(item, 1);
            }
            return RedirectToAction("Index", new { returnUrl });
        }
        public RedirectToRouteResult RemoveFromCart(int itemId, string returnUrl)
        {
            Items item = db.Item
                .FirstOrDefault(i => i.Id == itemId);
            if (item != null)
            {
                GetCart().RemoveLine(item);
            }
            return RedirectToAction("Index", new { returnUrl });
        }
    }
}