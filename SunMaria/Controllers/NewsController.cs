﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SunMaria.Controllers
{
    public class NewsController : Controller
    {
        // GET: News
        public ActionResult AllNews()
        {
            return View();
        }
        public ViewResult IdNews(int? id)
        {
            if (id == null)
            {
                return View("AllNews");
            }
            string idS = Convert.ToString(id);
            return View(idS);
        }
    }
}