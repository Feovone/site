using System.Linq;
using System.Web.Mvc;
using SunMaria.Areas.Item.Models;

namespace SunMaria.Controllers
{
    public class CategoryController : Controller
    {
        private readonly ItemContext db = new ItemContext();

        public ActionResult Index()
        {
            return PartialView("_CategoryMenu", db.Categories.ToList());
        }
    }
}